import React, { useEffect, useRef } from "react";
import * as maptalks from "maptalks";
import TrailData from "./trail.json";

const routeArray = TrailData.route.split(", ");
const routeData = [];
routeArray.map((route) => routeData.push(route.split(" ").map(Number)));

const IndoorMap = () => {
  const mapRef = useRef(null);

  useEffect(() => {
    const map = new maptalks.Map(mapRef.current, {
      center: [100.528635, 13.7290491],
      zoom: 15,
      baseLayer: new maptalks.TileLayer("base", {
        urlTemplate:
          "https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
        subdomains: ["a", "b", "c", "d"],
        attribution: ""
      })
    });

    const layer = new maptalks.VectorLayer("vector").addTo(map);
    const markers = [];
    TrailData?.features?.map((feature) =>
      markers.push(
        new maptalks.Marker(
          [feature.geometry.coordinates[0], feature.geometry.coordinates[1]],
          {
            symbol: {
              textFaceName: "sans-serif",
              textName: feature.properties.name.en,
              textFill: "#34495e",
              textHorizontalAlignment: "center",
              textSize: 30
            }
          }
        )
      )
    );

    layer.addGeometry(markers);

    const line = new maptalks.LineString(routeData, {
      arrowStyle: null, // arrow-style : now we only have classic
      arrowPlacement: "vertex-last", // arrow's placement: vertex-first, vertex-last, vertex-firstlast, point
      visible: true,
      editable: true,
      cursor: null,
      draggable: false,
      dragShadow: false, // display a shadow during dragging
      drawOnAxis: null, // force dragging stick on a axis, can be: x, y
      symbol: {
        lineColor: "#1bbc9b",
        lineWidth: 3
      }
    }).addTo(layer);

    // Clean up
    return () => {
      map.remove();
    };
  }, []);

  return <div ref={mapRef} style={{ width: "100vw", height: "100vh" }} />;
};

export default IndoorMap;
