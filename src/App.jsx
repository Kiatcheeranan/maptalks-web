import IndoorMap from "./components/indoormap/IndoorMap";

function App() {
  return <IndoorMap />;
}

export default App;
